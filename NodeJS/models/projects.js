const db = require('./db');
const Sequelize = db.Sequelize;
const sequelize = db.sequelize;

const util = require('../util');
const UserModel = require('./user');
const User = UserModel.User;

const ProjectImagesComments = sequelize.define('projectImagesComments', {
    uid: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    comment: {
        type: Sequelize.TEXT,
        allowNull: false
    }
});

const ProjectImages = sequelize.define('projectImages', {
    uid: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    image: {
        type: Sequelize.TEXT,
        allowNull: true
    }
});

const Projects = sequelize.define('projects', {
    uid: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    address: {
        type: Sequelize.STRING,
        allowNull: false
    },
    description: {
        type: Sequelize.TEXT,
        allowNull: false
    },
    primary_image: {
        type: Sequelize.TEXT,
        allowNull: true
    }
}, {
    hooks: {
        beforeUpdate: (instance, options) => {
            util.log(options, 'beforeUpdate')
        },
        afterUpdate: (instance, options) => {
            util.log(options, 'afterUpdate')
        },
        beforeDestroy: (instance, options) => {
            util.log(options, 'beforeDestroy')
        },
        beforeBulkDestroy: (options) => {
            util.log(options, 'options [ beforeBulkDestroy ]');

            if (options && options.where) {
                const uid = options.where.uid;
                ProjectImages.findAll({
                    where: { projectUid: uid}
                }).then(images => {
                    if (images.length > 0) {
                        const imageArray = [];
                        images.forEach(i => {
                            if (i.dataValues && i.dataValues.image) {
                                imageArray.push(i.dataValues.image)
                            }
                        });
    
                        util.log(imageArray, 'imageArray [ beforeBulkDestroy ]');
    
                        if (imageArray.length) {
                            util.removeImages(imageArray)
                        }
                    } else {
                        util.log(images, 'No Images [ beforeBulkDestroy ]');
                    }
                })
            } else {
                util.log(options, 'afterBulkDestroy [ ERROR ]')
            }
        },
    }
});

Projects.hasMany(ProjectImages, { onDelete: 'cascade', hooks: true });
ProjectImages.hasMany(ProjectImagesComments, {
    foreignKey: 'projectImageUID',
    sourceKey: 'uid',
    onDelete: 'cascade',
    hooks: true
});

ProjectImagesComments.belongsTo(User, {foreignKey: 'userUID', sourceKey: 'uid'});

Projects.prototype.createProjectImages = function(images) {
    if (images && images.constructor.name.toLowerCase() === 'array') {
        ProjectImages.bulkCreate(images.map(i => {
            return {image: i, projectUid: this.uid}
        }))
    } else {
        util.log(images, 'ProjectImages params is not an array! [ bulkCreate ]')
    }
};

sequelize.sync()
    .then(function(){
        console.log('Projects table has been successfully created, if one doesn\'t exist')
    })
    .catch(function(error){
        console.log('This error occured', error)
    });

module.exports = {
    Projects: Projects,
    ProjectImages: ProjectImages,
    ProjectImagesComments: ProjectImagesComments
};