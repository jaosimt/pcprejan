const db = require('./db');
const Sequelize = db.Sequelize;
const sequelize = db.sequelize;

const bcrypt = require('bcrypt');

const UserRole = sequelize.define('userRole', {
    uid: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    roleName: {
        type: Sequelize.STRING,
        allowNull: false
    }
});

const User = sequelize.define('users', {
    uid: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    token: {
        type: Sequelize.TEXT,
        unique: true,
        allowNull: false
    },
    user_name: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false
    },
    first_name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    last_name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    email: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
    },
    image: {
        type: Sequelize.TEXT,
        allowNull: true
    }
}, {
    hooks: {
        beforeCreate: function(user) {
            const salt = bcrypt.genSaltSync();
            user.password = bcrypt.hashSync(user.password, salt);
        }
    }
});

User.belongsTo(UserRole, {foreignKey: 'role', sourceKey: 'uid'});

User.prototype.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

// create all the defined tables in the specified database.
sequelize.sync()
    .then(function(){
        UserRole.findOrCreate({
            where: {uid: '0'},
            defaults: { roleName: 'Guest' }
        });
        UserRole.findOrCreate({
            where: {uid: '1'},
            defaults: { roleName: 'User' }
        });
        UserRole.findOrCreate({
            where: {uid: '2'},
            defaults: { roleName: 'Admin' }
        });
        
        console.log('User table has been successfully created, if one doesn\'t exist')
    })
    .catch(function(error){
        console.log('This error occured', error)
    });

// export User model for use in other files.
module.exports = {
    User: User,
    UserRole: UserRole
};
