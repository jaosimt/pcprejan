const Sequelize = require('sequelize');
const sequelize = new Sequelize('postgres://pcprejan@localhost:5432/pcprejan', {
    operatorsAliases: false
});

module.exports = {
    Sequelize: Sequelize,
    sequelize: sequelize
};