const UserModel = require('./models/user');
const User = UserModel.User;

const authenticate = (p) => {
    const params = {
        token: p && p.token ? p.token : '',
        user_name: p && p.user_name ? p.user_name : ''
    };
    return findByToken(params.token).then((user) => {
        if (user && (user.user_name === params.user_name)) {
            return {
                isAuthenticated: true,
                isUser: user.role > 0,
                isAdmin: user.role === 2,
                user: user
            }
        } else {
            return {
                isAuthenticated: false
            }
        }
    })
};

const findByToken = (token) => {
    return User.findOne({
        where: { token: token }
    }).then((user) => {
        return user ? user.dataValues : null;
    })
};

module.exports = {
    authenticate: authenticate,
    findByToken: findByToken
};