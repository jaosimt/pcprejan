const express = require('express');
const path = require('path');
const logger = require('morgan');

const indexRouter = require('./routes/index');
const util = require('./util');

const app = express();

const allowCrossDomain = function (req, res, next) {
    res.header('Content-Type', 'application/json');
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
};

app.use(allowCrossDomain);

//  app.use(logger('dev'));
app.use(logger('combined'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, 'public')));

app.use('/api', indexRouter);

//  catch 404 and forward to error handler
app.use(function(request, response, next) {
    next(util.status.notFound);
});

//  error handler
app.use(function(error, request, response, next) {
    util.log(next, 'Error Handler');
    response
        .status(util.status.notFound)
        .json('The Requested resource was not found in this server')
});

module.exports = app;
