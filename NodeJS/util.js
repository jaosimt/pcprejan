const fs = require('fs');

const requestStatus = {
    ok: 200,
    created: 201,
    updated: 202,
    noContent: 204,
    badRequest: 400,
    unauthorized: 401,
    notFound: 404,
    internalServerError: 500
};

const toCamelCase = (projectName) => {
    if (!projectName) { return '' }
    
    return projectName.toLowerCase().replace(/ (.)/g, (m, p) => {
        return p.toUpperCase()
    })
};

const consoleLog = (obj, label) => {
    if (typeof label === 'string') {
        console.log('-'.repeat(label.length));
        console.log(label);
        console.log('-'.repeat(label.length));
    }
    console.log(obj);
};

const removeImages = (image) => {
    if (image) {
        const removeImage = (img) => {
            fs.unlink(img, (error) => {
                if (error) {
                    consoleLog(error, "removeImages [ ERROR ]");
                    return
                }
                
                consoleLog(img, "removeImages [ DELETED ]");
            });
        };
    
        if (image.constructor.name.toLowerCase() === 'array') {
            image.forEach(i => {
                removeImage('public/' + i);
            })
        } else {
            removeImage('public/' + image);
        }
    } else {
        consoleLog("removeImages [ PARAMS image FALSE ]");
    }
};

const capitalized = (string) => {
    return string.replace(/^\w| \w/igm, (a) => {
        return a.toUpperCase()
    })
};

module.exports = {
    status: requestStatus,
    toCamelCase: toCamelCase,
    log: consoleLog,
    removeImages: removeImages,
    capitalized: capitalized
};
