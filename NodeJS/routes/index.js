const express = require('express');
const router = express.Router();

const projects = require('./projects');
router.get('/projects', projects.getAllProjects);
router.get('/project/:uid', projects.getSingleProject);
router.get('/project/search/:search', projects.searchProjects);
router.post('/project', projects.createProject);
router.put('/project/:uid', projects.updateProject);
router.delete('/project/:uid', projects.removeProject);

const projectImagesComments = require('./projectImagesComments');
router.post('/project/image/comment', projectImagesComments.createComment);


const users = require('./users');
router.get('/user/roles', users.getAllUserRoles);
router.post('/user/signup', users.signup);
router.post('/user/logout', users.logout);
router.post('/user/login', users.login);
router.post('/user/auth', users.auth);


const images = require('./images.js');
router.get('/image/base64/:uriEncodedImagePath', images.getBase64Image);
router.post('/image/upload/projectImages/:projectName/:userName/:token', images.uploadProjectImages);
router.post('/image/upload/profileImage/:userName', images.uploadProfileImage);


module.exports = router;