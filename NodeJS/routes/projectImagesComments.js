const ProjectModel = require('../models/projects');
const ProjectImagesComments = ProjectModel.ProjectImagesComments;

const auth = require('../auth');
const util = require('../util');
const authenticate = auth.authenticate;

const createComment = (request, response) => {
    authenticate(request.body.sessionUser).then(authRequest => {
        if (authRequest.isAuthenticated) {
            const params = {
                comment: request.body.comment,
                projectImageUID: request.body.projectImageUid,
                userUID:request.body.userUid
            };
    
            ProjectImagesComments.create(params)
                .then(comment => {
                    response
                        .status(util.status.created)
                        .json(comment.dataValues);
                })
                .catch(error => {
                    response
                        .status(util.status.internalServerError)
                        .json(error)
                });
        } else {
            response
                .status(util.status.unauthorized)
                .json();
        }
    });
};

module.exports = {
    createComment: createComment
};