const multer = require('multer');
const imageDataURI = require('image-data-uri');

const auth = require('../auth');
const util = require('../util');
const authenticate = auth.authenticate;

let projectName = 'unspecified';
let profileName = 'unspecified';

const projectStore = multer({
    storage: multer.diskStorage({
        destination: 'public/uploads/project/projectImages',
        filename: function (request, file, callback) {
            callback(null, util.toCamelCase(projectName) + "-" + file.originalname)
        }
    })
}).array('projectImages');

const profileStore = multer({
    storage: multer.diskStorage({
        destination: 'public/uploads/project/profileImages',
        filename: function (request, file, callback) {
            callback(null, util.toCamelCase(profileName) + "-" + file.originalname)
        }
    })
}).array('profileImage');

const getBase64Image = (request, response) => {
    imageDataURI.encodeFromFile('public/' + decodeURIComponent(request.params.uriEncodedImagePath)).then(res => {
        response.send(res)
    }).catch(error => {
        imageDataURI.encodeFromFile('public/images/no-image.jpg').then(res => {
            response.send(res)
        })
    });
};

const uploadProjectImages = (request, response) => {
    projectName = request.params.projectName;
    authenticate({
        user_name: request.params.userName,
        token: request.params.token
    }).then((authRequest) => {
        if (authRequest.isAuthenticated && authRequest.isUser) {
            projectStore(request, response, function (error) {
                if (error) {
                    response
                        .status(util.status.internalServerError)
                        .json(error);
                } else {
                    response
                        .status(util.status.ok)
                        .json(request.files);
                }
            })
        } else {
            response
                .status(util.status.unauthorized)
                .json();
        }
    })
};

const uploadProfileImage = (request, response) => {
    profileName = request.params.userName;
    profileStore(request, response, (error) => {
        if (error) {
            response
                .status(util.status.internalServerError)
                .json(error);
        } else {
            response
                .status(util.status.ok)
                .json(request.files);
        }
    })
};

module.exports = {
    getBase64Image: getBase64Image,
    uploadProjectImages: uploadProjectImages,
    uploadProfileImage: uploadProfileImage
};
