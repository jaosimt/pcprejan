const ProjectModel = require('../models/projects');
const UserModel = require('../models/user');

const Project = ProjectModel.Projects;
const ProjectImages = ProjectModel.ProjectImages;
const ProjectImagesComments = ProjectModel.ProjectImagesComments;
const User = UserModel.User;

const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const auth = require('../auth');
const util = require('../util');
const authenticate = auth.authenticate;

function getAllProjects(request, response) {
    Project.findAll({
        include: [{
            model: ProjectImages,
            include: [{
                model: ProjectImagesComments,
                include: {
                    model: User,
                    attributes: ['image']
                }
            }]
        }],
        order: [
            ['uid', 'DESC'],
            [ProjectImages, 'uid', 'ASC'],
            [ProjectImages, ProjectImagesComments, 'uid', 'DESC'],
        ]
    }).then(result => {
        response
            .status(util.status.ok)
            .json(result);
    }).catch(error => {
        response
            .status(util.status.internalServerError)
            .json(error)
    });
}

function createProject(request, response) {
    authenticate(request.body.sessionUser).then(authRequest => {
        if (authRequest.isAuthenticated && authRequest.isUser) {
            const params = {
                name: request.body.name,
                address: request.body.address,
                description:request.body.description,
                primary_image: request.body.primaryImage,
                images: request.body.images
            };
            
            Project.create(params)
                .then(project => {
                    if (project && project.dataValues) {
                        project.createProjectImages(params.images);
                        
                        response
                            .status(util.status.created)
                            .json(project.dataValues);
                    } else {
                        response
                            .status(util.status.internalServerError)
                            .json(project);
                    }
                })
                .catch(error => {
                    response
                        .status(util.status.internalServerError)
                        .json(error)
                });
        } else {
            response
                .status(util.status.unauthorized)
                .json();
        }
    });
}

function searchProjects(request, response) {
    let searchString = '%' + request.params.search + '%';
    Project.findAll({
        where: {
            [Op.or]: {
                name: {
                    [Op.iLike]: searchString
                },
                address: {
                    [Op.iLike]: searchString
                },
                description: {
                    [Op.iLike]: searchString
                }
            }
        },
        include: [{
            model: ProjectImages,
            include: [{
                model: ProjectImagesComments,
                include: [{
                    model: User
                }]
            }]
        }],
        order: [
            ['createdAt', 'DESC']
        ]
    }).then(result => {
        response
            .status(util.status.ok)
            .json(result);
    }).catch(error => {
        response
            .status(util.status.internalServerError)
            .json(error)
    });
}

function getSingleProject(request, response) {
    const uid = parseInt(request.params.uid);
    
    Project.findOne({
        include: [{
            model: ProjectImages,
            include: [{
                model: ProjectImagesComments,
                include: [{
                    model: User
                }]
            }]
        }],
        where: { uid: uid }
    }).then(result => {
        response
            .status(util.status.ok)
            .json(result);
    }).catch(error => {
        response
            .status(util.status.internalServerError)
            .json(error)
    });
}

function updateProject(request, response, next) {
    authenticate(request.body.sessionUser).then((authRequest) => {
        if (authRequest.isAuthenticated && authRequest.isUser) {
            Project.update({
                name: request.body.name,
                address: request.body.address,
                description: request.body.description
            }, {
                returning: "*",
                where: { uid: parseInt(request.body.uid) }
            }).then(([ rowsUpdate, [updatedProject] ]) => {
                if (updatedProject && updatedProject.dataValues) {
                    response
                        .status(util.status.updated)
                        .json(updatedProject.dataValues);
                } else {
                    response
                        .status(util.status.internalServerError)
                        .json(project);
                }
            });
        } else {
            response
                .status(util.status.unauthorized)
                .json();
        }
    });
}

function removeProject(request, response) {
    authenticate(request.body.sessionUser).then((authRequest) => {
        if (authRequest.isAuthenticated && authRequest.isAdmin) {
            const uid = parseInt(request.params.uid);
            
            Project.destroy({
                returning: "*",
                where: { uid: uid }
            }).then(result => {
                response
                    .status(util.status.ok)
                    .json(result);
            }).catch(error => {
                response
                    .status(util.status.internalServerError)
                    .json(error)
            });
        } else {
            response
                .status(util.status.unauthorized)
                .json();
        }
    });
}

module.exports = {
    getAllProjects: getAllProjects,
    getSingleProject: getSingleProject,
    createProject: createProject,
    updateProject: updateProject,
    removeProject: removeProject,
    searchProjects: searchProjects
};