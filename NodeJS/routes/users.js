const UserModel = require('../models/user');
const User = UserModel.User;
const UserRole = UserModel.UserRole;

const crypto = require('crypto');

const util = require('../util');
const auth = require('../auth');
const authenticate = auth.authenticate;
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const signup = (request, response) => {
    const user = {
        first_name: request.body.firstName,
        last_name: request.body.lastName,
        user_name: request.body.userName,
        role: request.body.role,
        email: request.body.email,
        password: request.body.password,
        image: request.body.image
    };
    
    const sessionUser = request.body.sessionUser;
    
    createToken()
        .then(token => user.token = token)
        .then(() => createUser(user, sessionUser))
        .then(user => {
            delete user.password;
            user.isAuthenticated = true;
            user.isUser = user.role > 0;
            user.isAdmin = user.role === 2;
            response.status(util.status.created).json(user)
        })
        .catch((error) => {
            response
                .status(util.status.internalServerError)
                .json(error);
        })
};

const logout = (request, response) => {
    const req = {
        user_name: request.body.user_name
    };
    let user;
    
    findUser(req)
        .then(foundUser => {
            if (foundUser) {
                user = foundUser.dataValues;
                return true
            } else {
                response
                    .status(util.status.notFound)
                    .json();
                return Promise.reject(util.status.notFound)
            }
        })
        .then((res) => createToken())
        .then(token => updateUserToken(token, user))
        .then(user => {
            response
                .status(util.status.ok)
                .json()
        })
        .catch((error) => {
            response
                .status(util.status.internalServerError)
                .json(error);
        })
};

const login = (request, response) => {
    const req = {
        user_name: request.body.userName,
        password: request.body.password
    };
    
    let user;
    
    findUser(req)
        .then(foundUser => {
            if (foundUser) {
                user = foundUser.dataValues;
                
                if (foundUser.validPassword(req.password)) {
                    return true
                } else {
                    response
                        .status(util.status.unauthorized)
                        .json();
                    return Promise.reject(util.status.unauthorized)
                }
            } else {
                response
                    .status(util.status.unauthorized)
                    .json();
                return Promise.reject(util.status.unauthorized)
            }
        })
        .then((res) => createToken())
        .then(token => updateUserToken(token, user))
        .then(user => {
            delete user.password;
            user.isAuthenticated = true;
            user.isUser = user.role > 0;
            user.isAdmin = user.role === 2;
            response.status(util.status.ok).json(user)
        })
        .catch((error) => {
            response
                .status(util.status.internalServerError)
                .json(error);
        })
};

const getAuth = (request, response) => {
    authenticate(request.body).then((authRequest) => {
        if (authRequest.isAuthenticated) {
            response
                .status(util.status.ok)
                .json(authRequest);
        } else {
            response
                .status(util.status.unauthorized)
                .json();
        }
    });
};

const findUser = (req) => {
    return User.findOne({
        where: {
            [Op.or]: [{user_name: req.user_name}, {email: req.user_name}]
        }
    }).then((user) => user)
};

const createUser = (user, sessionUser) => {
    return new Promise((resolve, reject) => {
        authenticate(sessionUser).then((authRequest) => {
            if (authRequest.isAuthenticated) {
                if (!authRequest.isAdmin) {
                    user.role = 0;
                }
            } else {
                // Comment out below line if creating admin user the first time (not authenticated)
                //user.role = 0;
            }
            user.first_name = util.capitalized(user.first_name);
            user.last_name = util.capitalized(user.last_name);
            
            User.create(user).then((user) => {
                resolve(user.dataValues)
            }).catch(error => {
                reject(error);
            });
        })
    });
};

const updateUserToken = (token, user) => {
    return User.update({token: token}, {
        returning: "*",
        where: { uid: user.uid }
    }).then(([ rowsUpdate, [updatedUser] ]) => updatedUser.dataValues);
};

const createToken = () => {
    return new Promise((resolve, reject) => {
        crypto.randomBytes(16, (err, data) => {
            err ? reject(err) : resolve(data.toString('base64').replace(/[/?#]/g, '\$'))
        })
    })
};

function getAllUserRoles(request, response) {
    UserRole.findAll({
        order: [
            ['uid', 'ASC']
        ]
    }).then(result => {
        response
            .status(util.status.ok)
            .json(result);
    }).catch(error => {
        response
            .status(util.status.internalServerError)
            .json(error)
    });
}

module.exports = {
    getAllUserRoles: getAllUserRoles,
    signup: signup,
    logout: logout,
    login: login,
    auth: getAuth
};