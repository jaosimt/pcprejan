function fbEmbed(){
    simo.after(500, function(){
        const leftContent = document.querySelector('.content-left'),
            centerContent = document.querySelector('.content-center');
        
        leftContent.innerHTML = '<div class="fb-page" data-href="https://www.facebook.com/pcprejan/" data-tabs="timeline" data-small-header="true" data-width="' + parseInt(Math.min(leftContent.getBoundingClientRect().width, 340)) + 'px" data-height="' + parseInt(Math.max(centerContent.getBoundingClientRect().height - 112, 600)) + 'px" data-hide-cover="false" data-show-facepile="fales"></div>';
        
        if (FB) {
            FB.init({
                appId            : '191609164213909',
                xfbml            : true,
                version          : 'v3.0'
            });
        }
    })
}

function getSessionUser(){
    const sessionUser = JSON.parse(simo.get.storedItem("pcpSessionUser"));
    
    //  ToDo: Session Expire should be handled in the backend
    if (sessionUser && ((new Date() - new Date(sessionUser.updatedAt)) / (60*60*60*60) <= 1)) {
        return sessionUser
    } else {
        log("expired session (1hr)");
        simo.remove.storedItem("pcpSessionUser");
        return null
    }
}

function getAllProjects(activeProjectUid, callback){
    simo.get.data(simo.pcprejanApiUrl + "/projects?t=" + simo.get.timeStamp(), function(response) {
        simo.dataHandler(response, function(json){
            simo.pcp.projects = json || [];
            if (simo.is.number(activeProjectUid)) {
                simo.pcp.active.project = simo.pcp.projects.getWithUID(activeProjectUid);
            }
            loadProject();
            if (simo.is.function(callback)) {
                callback()
            }
        }, null, true)
    }, true)
}

function loadFileContent(htmlFile, container, callback) {
    const requestUrl = htmlFile + "?t=" + simo.get.timeStamp();
    simo.get.data(requestUrl, function(response){
        simo.dataHandler(response, function(htmlString){
            htmlString = htmlString.replace(/<body/, "<sImo><body").replace(/<\/body>/, "</body></sImo>").split(/(<sImo>|<\/sImo>)/ig)[2];
            
            if (simo.is.htmlObject(container)) {
                container.innerHTML = htmlString;
            }
    
            if (simo.is.function(callback)) {
                callback(htmlString);
            }
            simo.init.uiWidgets();
        }, function(){
            const notFoundString = '<h1>Not Found</h1>' +
                '<h5 style="font-weight: normal;">The Requested URL <span style="font-style: italic;">' +
                '<strong>' + requestUrl + '</strong></span> was not found on this server</h5>';
            
            if (simo.is.htmlObject(container)) {
                container.innerHTML = notFoundString
            }
    
            if (simo.is.function(callback)) {
                callback(notFoundString);
            }
            simo.init.uiWidgets();
        }, true);
    }, true)
}

function initMenuClicks(){
    const menuItem = document.querySelectorAll('.menu-item .item:not(.selected)');
    
    if (menuItem.length > 0) {
        menuItem.forEach(function (t) {
            t.onclick = function(){
                if (this.classList.contains('selected')) {
                    return;
                }
                
                const projectMarker = document.querySelector('.menu-item .item.project .marker'),
                    projectLabel = document.querySelector('.menu-item .item.project .label'),
                    selected = document.querySelector('.menu-item .item.selected');
                
                const mb = projectLabel.getBoundingClientRect();
                
                if (simo.is.htmlObject(selected)) {
                    const clickedLabel = this.querySelector('.label');
                    const thisB = clickedLabel.getBoundingClientRect();
                    const thisLeft = thisB.x - mb.x;
                    
                    projectMarker.style.transform = 'translateX(' + (thisLeft < 0 ? 0 : thisLeft) + 'px)';
                    projectMarker.style.width = thisB.width + 'px';
                    
                    selected.classList.remove('selected');
                    this.classList.add('selected');
                    
                    initMenuClicks();
                }
    
                const targetEL = document.querySelector('.content-center'),
                    targetDF = this.getAttribute("data-file");
    
                loadFileContent(targetDF, targetEL, function(htmlString) {
                    if (targetDF === "projects.html") {
                        if (simo.is.htmlObject(targetEL)) {
                            targetEL.classList.add("project");
                            getAllProjects();
                            checkAuth((auth) => {
                                updateAuthElements(auth && auth.isAuthenticated);
                            });
                        }
                    }
                });
            }
        })
    }
}

function showModal(title, cssClass, content){
    (function(d, s, name, title, cssClass, content, extraAttr) {
        let div, body = d.getElementsByTagName("body")[0];
        div = d.createElement(s);
        div.setAttribute("name", name);
        div.setAttribute("title", title);
        div.setAttribute("class", cssClass);
        div.setAttribute(extraAttr, extraAttr);
        div.innerHTML = content;
        body.append(div);
    }(document, 'div', 'sImo-ui-modal', title, cssClass, content, 'showCloseIcon'));
    
    simo.init.uiWidgets();
}

function closeModal(e){
    const modal = simo.get.parentNode('sImo-ui-modal', e);
    if (simo.is.htmlObject(modal)) {
        const modalCloseButton = modal.querySelector('.modal-close-button');
        if (simo.is.htmlObject(modalCloseButton)) {
            modalCloseButton.click();
        }
    }
}

function validateSignup(e){
    let incomplete = false,
        d = simo.get.formData(simo.get.parentNode('pcp-signup', e));
    
    for (const i in d) {
        if (d.hasOwnProperty(i) && simo.is.empty(d[i]) && i !== 'profileImage') {
            incomplete = true;
        }
    }
    
    const pcpSignup = simo.get.parentNode('pcp-signup', e);
    const btn = pcpSignup.querySelector('button.signup'),
        cp = pcpSignup.querySelector('input[name=confirmPassword]');
    
    if (!simo.is.empty(d.password) && !simo.is.empty(d.confirmPassword) && d.password !== d.confirmPassword) {
        incomplete = true;
        cp.classList.add('error');
    } else {
        cp.classList.remove('error');
    }
    
    if (simo.is.htmlObject(btn)) {
        if (!incomplete) {
            btn.classList.remove('disabled');
        } else {
            btn.classList.add('disabled');
        }
    }
}

function getDataUID(e) {
    const list = simo.get.parentNode("list", e);
    if (simo.is.htmlObject(list)) {
        let uid = (list.getAttribute("data-uid"));
        if (simo.is.numeric(uid)) {
            return parseInt(uid)
        }
    }
    
    return null;
}

function getListItem(e) {
    const list = simo.get.parentNode("list", e);
    if (simo.is.htmlObject(list)) {
        let uid = (list.getAttribute("id").match(/\d+$/) || [])[0];
        let name = list.querySelector('p.name');
        if (simo.is.numeric(uid)) {
            return {
                uid: parseInt(uid),
                name: name ? (name.innerText || null) : null
            };
        }
    }
    
    return null;
}

function addProject(){
    loadFileContent('add.html', null, (htmlString) => {
        let container = document.querySelector('.content-center');
        if (simo.is.htmlObject(container)) {
            container.innerHTML += htmlString;
        }
    });
}

function listEditClick(e){
    if (!hasProject(e)) {
        return
    }
    
    let container = document.querySelector('.content-center');
    if (simo.is.htmlObject(container)) {
        loadFileContent('edit.html', null, (htmlString) => {
            container.innerHTML += htmlString;
            
            simo.after(100, function(){
                const editProject = container.querySelector('.edit-project');
                editProject.querySelector('[name=name]').value = simo.pcp.active.project.name;
                editProject.querySelector('[name=address]').value = simo.pcp.active.project.address;
                editProject.querySelector('[name=description]').value = simo.pcp.active.project.description;
                
                const projectImage = editProject.querySelector('.edit-project-images .images-wrapper');
                (simo.pcp.active.project.projectImages || []).forEach(i => {
                    projectImage.innerHTML += '<div class="project-image" style="background-image: url(' + simo.apiServer + '/' + i.image + ')"></div>';
                })
            });
        })
    }
}

function listDeleteClick(e){
    if (!hasProject(e)) {
        return
    }
    
    showModal('<strong>Are you sure?</strong>', 'rounded red fixed',
        '<center>You are about to delete project <strong>' + simo.pcp.active.project.name + '</strong> with <strong>ID: ' + simo.pcp.active.project.uid + '</strong>' +
        '   <br/><br/>' +
        '   <button class="red" onclick=projectDeleteClick(this)>DELETE</button>' +
        '   <button onclick=closeModal(this)>Cancel</button>' +
        '</center>');
}

function updateAuthElements(status){
    let loginButton = document.querySelector('.pcp-login-button');
    let loginScreen = document.querySelector('.pcp-login');
    let sessionUserWelcome = document.querySelector('.pcp-session-user');
    let addUser = document.querySelector('.add-pcp-user');
    let addProjectButton = document.querySelector('.add-project-button');
    let listIcns = document.querySelectorAll('.list .list-icons');
    
    if (status === "success" || status === true){
        isAuthenticated = true;
        
        listIcns.forEach((icn) => {
            if (icn.classList.contains('hidden')) {
                icn.classList.remove("hidden")
            }
        });
        
        if (simo.is.htmlObject(addProjectButton) && addProjectButton.classList.contains('hidden')) {
            addProjectButton.classList.remove('hidden');
        }
        
        if (simo.is.htmlObject(loginScreen) && !loginScreen.classList.contains('hidden')){
            loginScreen.classList.add("hidden");
        }
        
        if (simo.is.htmlObject(sessionUserWelcome) && sessionUserWelcome.classList.contains("hidden")) {
            sessionUserWelcome.classList.remove("hidden");
            
            let sessionUser = JSON.parse(simo.get.storedItem("pcpSessionUser"));
            if (sessionUser && sessionUser.first_name) {
                sessionUserWelcome.innerText = sessionUser.first_name;
            }
        }
        
        if (simo.is.htmlObject(addUser) && addUser.classList.contains("hidden")) {
            addUser.classList.remove("hidden");
        }
        
        if (simo.is.htmlObject(loginButton)) {
            loginButton.classList.add('on');
            loginButton.classList.remove('off');
        }
    } else {
        isAuthenticated = false;
        
        listIcns.forEach((icn) => {
            if (!icn.classList.contains('hidden')) {
                icn.classList.add("hidden")
            }
        });
        
        if (simo.is.htmlObject(addProjectButton) && !addProjectButton.classList.contains('hidden')) {
            addProjectButton.classList.add('hidden');
        }
        
        if (simo.is.htmlObject(loginScreen) && loginScreen.classList.contains('hidden')){
            loginScreen.classList.remove("hidden");
        }
        
        if (simo.is.htmlObject(sessionUserWelcome) && !sessionUserWelcome.classList.contains("hidden")) {
            sessionUserWelcome.classList.add("hidden");
        }
        
        if (simo.is.htmlObject(addUser) && !addUser.classList.contains("hidden")) {
            addUser.classList.add("hidden");
        }
    
        if (simo.is.htmlObject(loginButton)) {
            loginButton.classList.add('off');
            loginButton.classList.remove('on');
        }
    }
}

function showListAction(e) {
    if (simo.is.htmlObject(e)) {
        let icn = e.querySelector('.icn');
        
        if (simo.is.htmlObject(icn)) {
            icn.classList.remove("hidden");
        }
    }
}

function hideListAction(e) {
    if (simo.is.htmlObject(e)) {
        let icn = e.querySelector('.icn');
        
        if (simo.is.htmlObject(icn)) {
            icn.classList.add("hidden");
        }
    }
}

function loadProject(){
    const plist = document.querySelector(".content-list.project");
    if (simo.is.htmlObject(plist)) {
        plist.innerHTML = "";
        simo.pcp.projects.forEach(function (p) {
            let bg = '', d = new Date(p.createdAt.replace(/z$/i, ''));
            if (p.primary_image) {
                bg = ' style="background: rgba(220, 220, 220, 0.60) url(' + simo.apiServer + "/" + p.primary_image + ') no-repeat center; background-size: 100%"'
            }
            
            let icnAction = '';
            if (simo.is.mobile()) {
                icnAction = ' onclick="showListAction(this);"'
            } else {
                icnAction = ' onmouseover="showListAction(this);"'
            }
            
            icnAction += ' onmouseout="hideListAction(this);"';
            
            const listIcons =
                '<div class="transform list-icons float-right' + (!isAuthenticated ? ' hidden' : '') + '"' + icnAction + '> ' +
                '   <div class="icn transform hidden">' +
                '       <div class="icn-edit" onclick="listEditClick(this)">Edit</div>' +
                '       <div class="icn-delete" onclick="listDeleteClick(this)">Delete</div>' +
                '   </div>' +
                '</div>';
            
            plist.innerHTML +=
                '<div id="uid-' + p.uid + '" data-uid="' + p.uid + '" class="list transform">' +
                '   <p class="name">' + p.name + '</p>' +
                '   <p class="address">' + p.address + '</p>' + listIcons +
                '   <div class="image"' + bg + ' onclick="viewProject(this)"></div>' +
                '   <p class="description">' + p.description + '</p>' +
                '   <p class="time-stamp">' + d + '&nbsp;&nbsp;|&nbsp;&nbsp;' + dateAgo(d) + ' ago</p>' +
                '</div>'
        });
        
        simo.init.uiWidgets();
    }
}

function hasProject(e) {
    let projectUID = getDataUID(e);
    if (!simo.is.number(projectUID)) {
        simo.notification({
            message: 'Unable to obtain project uid on clicked item',
            autoHide: true,
            color: 'red'
        });
    
        return false
    }

    simo.pcp.active.project = simo.pcp.projects.getWithUID(projectUID);
    
    if (simo.is.empty(simo.pcp.active.project)) {
        simo.notification({
            message: 'Unable to obtain project data with ID ' + projectUID,
            autoHide: true,
            color: 'red'
        });
    
        return false
    }
    
    return true;
}

function viewProject(e) {
    if (!hasProject(e)) {
        return
    }
    
    simo.coverOverlay({
        action: 'show',
        backgroundRGB: '0, 0, 0',
        opacity: 0.95
    });
    
    let imgs = simo.pcp.active.project.projectImages.clone();
    imgs = imgs.map(i => {
        i.image = simo.apiServer + '/' + i.image;
        return i
    });
    
    const coverOverlay = document.querySelector('.simo-overlay');
    if (simo.is.htmlObject(coverOverlay)) {
        coverOverlay.style.zIndex = 999;
        
        loadFileContent("viewProject.html", coverOverlay, (htmlString) => {
            const sessionUser = getSessionUser();
            
            coverOverlay.querySelector('.pcp-view-project .details .title .name').innerText = simo.pcp.active.project.name;
            coverOverlay.querySelector('.pcp-view-project .details .title .address').innerText = simo.pcp.active.project.address;
            coverOverlay.querySelector('.pcp-view-project .details .project-description').innerText = simo.pcp.active.project.description;
    
            const commentSection = coverOverlay.querySelector('.pcp-view-project .details .comment-section');
            const addComments = commentSection.querySelector('.add-comments');
    
            if (sessionUser) {
                addComments.classList.remove('hidden');
                addComments.querySelector('.user-image').style.backgroundImage = 'url(' + simo.apiServer + '/' + sessionUser.image + ')';
                addComments.querySelector('.post').setAttribute('data-user-uid', sessionUser.uid);
            } else if (!addComments.classList.contains('hidden')){
                addComments.classList.add('hidden');
            }
    
            function ___showCarousel(){
                showCarousel('.pcp-view-project .image', imgs, 5000, 0, false, false, (imageUID) => {
                    addComments.querySelector('.post').setAttribute('data-image-uid', imageUID);
    
                    updateImageComments(imageUID);
                });
            }
            
            ___showCarousel();
            
            const oldWindowOnResize = window.onresize;
            window.onresize = function() {
                if (simo.is.htmlObject(document.querySelector('.simo-overlay .pcp-view-project'))) {
                    ___showCarousel();
                }
                oldWindowOnResize();
            }
        })
    }
}

function updateImageComments(imageUID) {
    const userComments = document.querySelector('.pcp-view-project .details .comment-section .user-comments');
    if (!userComments) {
        return
    }
    
    userComments.innerHTML = '';
    
    if (!simo.is.number(imageUID)) {
        return
    }
    
    const currentImage = simo.pcp.active.project.projectImages.getWithUID(imageUID),
        createComment = (userComment, userImage) => {
            return '<div class="comments transform"><div class="user-image" style="background-image: url(' + (simo.apiServer + '/' + userImage) + ')"></div><div class="user-comment">' + userComment.replace(/[\n]/gm, '<br/>') + '</div></div>';
        };
    
    if (userComments && currentImage && simo.is.array(currentImage.projectImagesComments) && currentImage.projectImagesComments.length > 0) {
        currentImage.projectImagesComments.forEach(c => {
            userComments.innerHTML += createComment(c.comment, c.user.image);
        });
    }
}
function onSearchStringChanged(el) {
    if (simo.trim(el.value) === "") {
        simo.notification({
            message: "Loading [ Projects ]",
            autoHide: true,
            color: 'blue'
        });
        
        getAllProjects();
    }
}

function projectAddClick(e) {
    e.setAttribute('disabled', 'disabled');
    
    const params = simo.get.formData(document.querySelectorAll('.add-project'));
    if (simo.is.empty(params.name)) {
        simo.notification({
            message: "Required project name is missing!",
            autoHide: true,
            color: "red"
        });
        
        e.removeAttribute('disabled');
        return
    }
    
    const sessionUser = getSessionUser();
    params.sessionUser = sessionUser;
    
    const formData = new FormData(document.querySelector('#projectImages'));
    
    if (sessionUser){
        simo.send.data("POST", formData, simo.pcprejanApiUrl + "/image/upload/projectImages/" + params.name + "/" + sessionUser.user_name + "/" + sessionUser.token, function (response) {
            simo.dataHandler(response, (data) => {
                params.primaryImage = data[0].path.replace(/^public\//, '');
                const imagesList = [];
                data.forEach((i) => {
                    imagesList.push(i.path.replace(/^public\//, ''));
                });
    
                params.images = imagesList;
    
                simo.send.data("POST", params, simo.pcprejanApiUrl + "/project", function (res) {
                    simo.dataHandler(res, (json) => {
                        getAllProjects();
                        closeModal(e);
                    }, () => {
                        e.removeAttribute('disabled');
                    })
                }, true)
            }, () => {
                e.removeAttribute('disabled');
            })
        }, true)
    } else {
        simo.notification({
            message: "You are not authorized!",
            autoHide: true,
            color: "red",
        });
        
        closeModal(e);
    }
}

function projectUpdateClick(e){
    e.setAttribute('disabled', 'disabled');
    
    const params = simo.get.formData(document.querySelectorAll('.edit-project'));
    if (simo.is.empty(params.name)) {
        simo.notification({
            message: "Required project name is missing!",
            autoHide: true,
            color: "red"
        });
        
        e.removeAttribute('disabled');
        return
    }
    
    const sessionUser = getSessionUser();
    // const formData = new FormData(document.querySelector('#projectImages'));
    
    if (sessionUser) {
        const updatedParams = simo.pcp.projects.updateWithUID(simo.pcp.active.project.uid, params);
        updatedParams.sessionUser = sessionUser;
        
        simo.send.data("PUT", updatedParams, simo.pcprejanApiUrl + "/project/" + updatedParams.uid, function (res) {
            simo.dataHandler(res, (json) => {
                getAllProjects();
                closeModal(e);
            }, () => {
                e.removeAttribute('disabled');
            })
        }, true)
    }
    
    closeModal(e);
}

function projectDeleteClick(e){
    const uid = simo.pcp.active.project.uid;
    e.setAttribute('disabled', 'disabled');
    
    simo.send.data("DELETE", {sessionUser: getSessionUser()}, simo.pcprejanApiUrl + "/project/" + uid, function (res) {
        simo.dataHandler(res, (json) => {
            closeModal(e);
            
            simo.pcp.active.project = {};
            simo.pcp.projects.deleteWithUID(uid);
            
            const listItem = document.querySelector("#uid-" + uid);
            if (simo.is.htmlObject(listItem)) {
                listItem.classList.add("fadeOut");
                simo.after(500, function(){
                    simo.removeAll(listItem);
                })
            }
        })
    }, true)
}

function searchProject(btn){
    const searchString = simo.trim(btn.parentElement.querySelector('input').value);
    if (searchString !== "") {
        simo.get.data(simo.pcprejanApiUrl + "/project/search/" + searchString, function(response) {
            simo.dataHandler(response, function(json){
                simo.pcp.projects = json;
                loadProject();
            })
        }, true);
    } else {
        simo.notification({
            message: "Reloading Projects",
            autoHide: true,
            color: 'blue'
        });
        
        getAllProjects();
    }
}

function signUpClick(){
    loadFileContent('signup.html', null, function(htmlString){
        simo.get.data(simo.pcprejanApiUrl + "/user/roles", (response) => {
            simo.dataHandler(response, (json) => {
                let roleOptions = '';
                json.forEach((role) => {
                    roleOptions +=
                        '<option' + (role.uid === 0 ? ' selected="selected"' : '') +
                        ' value="' + role.uid + '">' + role.roleName +
                        '</option>';
                });
    
                htmlString = htmlString.replace(/<%\ *roleOptions\ *%>/gm, roleOptions);
                showModal('Signup', 'rounded red fixed pcp-signup', htmlString)
            }, null, true);
        }, true);
    });
}

function logout(e){
    if (!e.classList.contains('on')) {
        if (simo.is.mobile()) {
            loadFileContent('login.html', null, (htmlString) => {
                showModal('Login', 'red rounded fixed login-modal', htmlString);
            });
            
            return;
        }
        
        const loginScreen = document.querySelector('[name=sImo-ui-div][title=Login]');
        if (simo.is.htmlObject(loginScreen)) {
            const userNameInput = loginScreen.querySelector('[name=sImo-ui-div][title=Login] [name=userName]');
            if (simo.is.htmlObject(userNameInput)) {
                userNameInput.focus();
            }
            
            loginScreen.scrollIntoView({
                behavior: 'smooth'
            });
            loginScreen.classList.add('box-shadow-out-red');
            simo.after(2000, () => {
                loginScreen.classList.remove('box-shadow-out-red');
    
            })
        }
        return
    }
    
    showModal('<strong>Are you sure?</strong>', 'rounded red fixed',
        '<center>You are about to logout</strong>' +
        '   <br/><br/>' +
        '   <button class="red" onclick=executeLogout(this)>LOGOUT</button>' +
        '   <button onclick=closeModal(this)>Cancel</button>' +
        '</center>');
}

function executeLogout(e) {
    closeModal(e);
    let sessionUser = getSessionUser();
    if (sessionUser) {
        simo.send.data("POST", sessionUser, simo.pcprejanApiUrl + "/user/logout", function (response) {
            simo.dataHandler(response, (json) => {
                // nothing necessary
            })
        }, true);
    }
    
    simo.remove.storedItem("pcpSessionUser");
    updateAuthElements();
}

function checkAuth(callback){
    let sessionUser = getSessionUser();
    if (sessionUser) {
        simo.send.data("POST", sessionUser, simo.pcprejanApiUrl + "/user/auth", function (response) {
            if (simo.is.function(callback)) {
                simo.dataHandler(response, (json) => {
                    callback(json);
                }, () => {
                    callback(null);
                }, true)
            }
        }, true);
    } else {
        if (simo.is.function(callback)) {
            callback(null);
        }
        
        log("No session user [ checkAuth ]");
    }
}

function updateSessionUser(user) {
    user.updatedAt = new Date();
    simo.set.storeItem("pcpSessionUser", JSON.stringify(user));
    
    if (user && user.id) {
        updateAuthElements(user.isAuthenticated);
    } else {
        checkAuth((user) => {
            updateAuthElements(user && user.isAuthenticated);
        });
    }
}

function onSignupClick(e){
    e.classList.add('disabled');
    
    const modal = simo.get.parentNode('sImo-ui-modal', e);
    if (simo.is.htmlObject(modal)) {
        const params = simo.get.formData(modal.querySelector('.pcp-signup'), ['input', 'select']);
        
        log("\nsignup click");
        log("------------");
        
        if (params.password !== params.confirmPassword) {
            simo.notification({
                message: 'Password and Confirm Password differed!',
                color: 'red'
            });
        } else {
            const formData = new FormData(document.querySelector('#profileImages'));
            simo.send.data("POST", formData, simo.pcprejanApiUrl + "/image/upload/profileImage/" + params.userName, (response) => {
                simo.dataHandler(response, (data) => {
                    if (data.length) {
                        params.image = data[0].path.replace(/^public\//, '');
                        params.sessionUser = getSessionUser();
                    }
    
                    simo.send.data("POST", params, simo.pcprejanApiUrl + "/user/signup", function (user) {
                        simo.dataHandler(user, (json) => {
                            let modalCloseButton = modal.querySelector('.modal-close-button');
                            if (simo.is.htmlObject(modalCloseButton)) {
                                modalCloseButton.click();
                            }
    
                            updateSessionUser(json)
                        }, (response) => {
                            e.classList.remove('disabled');
                            // ToDo: Delete uploaded profile image
                        })
                    }, true);
                }, null, true);
            });
        }
    }
}

function login(e){
    const container = simo.get.parentNode('sImo-ui-div', e);
    if (simo.is.htmlObject(container)) {
        let hasError = false, login = simo.get.formData(container);
        
        if (simo.is.empty(login.userName)) {
            hasError = true;
            simo.notification({
                message: "Please Provide missing [ User Name ] login requirement",
                autoHide: true,
                color: "red"
            });
        } else if (simo.is.empty(login.password)) {
            hasError = true;
            simo.notification({
                message: "Please Provide missing [ User Password ] login requirement",
                autoHide: true,
                color: "red",
                secondsTimeOut: 5
            });
        }
        
        if (!hasError) {
            const inputs = container.querySelectorAll('input');
            inputs.forEach(function (t) {
                t.value = "";
            });
            
            simo.send.data('POST', login, simo.pcprejanApiUrl + "/user/login", function(response) {
                simo.remove.storedItem("pcpSessionUser");
                simo.dataHandler(response, (user) => {
                    if (user && user.uid && user.user_name && user.token) {
                        updateSessionUser(user);
                        if (simo.is.mobile()) {
                            closeModal(e);
                        }
                    } else {
                        simo.notification({
                            message: user.message || 'Error while trying to log you in',
                            color: 'red',
                            autoHide: true,
                            secondsTimeOut: 5
                        });
        
                        updateAuthElements("error");
                    }
                })
            }, true);
        }
    }
}

function showCarousel(container, imageArray, interval, widthOffset, autoPlay, glowOnScroll, callback){
    glowOnScroll = simo.is.boolean(glowOnScroll) ? glowOnScroll : true;
    autoPlay = simo.is.boolean(autoPlay) ? autoPlay : true;
    widthOffset = parseFloat(widthOffset) || 0;
    container = document.querySelector(container);
    
    let imageIsObject = false,
        firstLoad = true;
    
    if (simo.is.array(imageArray) && simo.is.htmlObject(container)) {
        const client = container.getBoundingClientRect();
        const cHeight = client.height,
            cWidth = client.width;
        
        const centerIndex = Math.floor(imageArray.length / 2);
        let images = [],
            currentLeft = -Math.abs(centerIndex * 100);
        
        function divImage(image, index, left){
            let img = image;
            if (simo.is.object(image)) {
                imageIsObject = true;
                img = image.image
            }
            const div = document.createElement('div');
            div.style.background = 'url(' + img + ') no-repeat center';
            div.setAttribute('id', 'slide-' + index);
            div.setAttribute('class', 'image-slide carousel float-left' + (left === 0 ? ' first-image' : ''));
            div.style.width = (cWidth + widthOffset) + "px";
            div.style.left = left + '%';
            div.style.height = cHeight + 'px';
            div.style.position = 'absolute';
            
            if (imageIsObject) {
                div.setAttribute('data-image-uid', image.uid);
            }
            
            return div;
        }
    
        function updateAddComments(hide){
            const addComments = document.querySelector('.pcp-view-project .details .comment-section .add-comments'),
                sessionUser = getSessionUser();
    
            if (!addComments) {
                return
            }
            
            if (!sessionUser || !imageIsObject) {
                addComments.classList.add('hidden');
                return
            }
            
            if (imageIsObject) {
                if (addComments) {
                    if (hide) {
                        addComments.classList.add('hidden');
                    } else {
                        addComments.classList.remove('hidden');
                    }
                }
            }
        }
        
        container.style.overflow = 'hidden';
        container.style.position = 'relative';
        container.innerHTML = "";
        
        imageArray.forEach(function (image, index) {
            let currentImage = divImage(image, index, currentLeft);
            container.append(currentImage);
            images.push(currentImage);
            currentLeft += 100;
        });
        
        let paused = false, direction = 0,  manual = !autoPlay,
            controls = document.createElement('div');
        
        controls.setAttribute('class', 'transform slide-control float-right hidden pause');
        controls.innerHTML =
            '<div class="control-button previous">«</div>' +
            '<div class="control-button play pause">' + (autoPlay ? '❚❚' : (direction === 1 ? '◄' : '►') )+ '</div>' +
            '<div class="control-button next">»</div>';
        
        container.append(controls);
        
        const previousButton = controls.querySelector('.previous'),
            playButton = controls.querySelector('.play'),
            nextButton = controls.querySelector('.next');
    
        previousButton.style.top = ((cHeight / 2) - previousButton.getBoundingClientRect().height - 10) + 'px';
        nextButton.style.top = ((cHeight / 2) - nextButton.getBoundingClientRect().height - 10) + 'px';
    
        updateAddComments(autoPlay);
    
        function loopMe(oramismo) {
            if (!autoPlay && controls.classList.contains('hidden')) {
                controls.classList.remove('hidden');
            }
            
            if (manual && !oramismo) {
                previousButton.classList.remove('disabled');
                nextButton.classList.remove('disabled');
                
                if (firstLoad) {
                    firstLoad = false;
                    simo.after(200, ()=>{
                        const firstImage = container.querySelector('.image-slide.first-image');
                        if (simo.is.function(callback) && simo.is.htmlObject(firstImage)) {
                            callback(parseInt(firstImage.getAttribute('data-image-uid')));
                        }
                    });
                }
                
                return
            }
            
            simo.after(oramismo === true ? 0 : (interval || 10000), function() {
                if ((!paused || manual) && simo.is.elementInViewport(container)) {
                    previousButton.classList.add('disabled');
                    nextButton.classList.add('disabled');
                    
                    if (controls.classList.contains('hidden')) {
                        controls.classList.remove('hidden');
                    }
    
                    const firstImage = images[0],
                        lastImage = images[images.length - 1];
    
                    const firstLeft = parseInt(firstImage.style.left),
                        lastLeft = parseInt(lastImage.style.left);
    
                    if (direction === 0) {
                        firstImage.classList.add('hidden');
                    } else {
                        lastImage.classList.add('hidden');
                    }
    
                    simo.after(1500, () => {
                        if (direction === 0) {
                            firstImage.classList.remove('hidden');
                        } else {
                            lastImage.classList.remove('hidden');
                        }
                    });
    
                    images.forEach((image, index) => {
                        const thisLeft = parseInt(image.style.left),
                            directionLeft = direction === 0 ? -100 : 100;
        
                        if (direction === 0) {
                            if (index === 0) {
                                image.style.left = lastLeft + '%';
                            } else {
                                image.style.left = (thisLeft + directionLeft) + '%'
                            }
                        } else {
                            if (index === (images.length - 1)) {
                                image.style.left = firstLeft + '%';
                            } else {
                                image.style.left = (thisLeft + directionLeft) + '%'
                            }
                        }
        
                        if ((thisLeft + directionLeft) === 0) {
                            const currentImage = image;
                            simo.after(750, () => {
                                if (simo.is.function(callback)) {
                                    callback(parseInt(currentImage.getAttribute('data-image-uid')));
                                }
    
                                simo.after(750, () => {
                                    if (glowOnScroll) {
                                        currentImage.classList.remove('slider-shadow');
                                    }
                                    previousButton.classList.remove('disabled');
                                    nextButton.classList.remove('disabled');
                                })
                            });
                        } else if (glowOnScroll) {
                            image.classList.add('slider-shadow');
                        }
                    });
    
                    if (direction === 0) {
                        const x = images.shift();
                        images.push(x);
                    } else {
                        const x = images.pop();
                        images.unshift(x); }
                }
                
                if (!manual && !paused) {
                    loopMe();
                }
            })
        }
        
        playButton.onclick = function() {
            if (this.classList.contains('pause')) {
                this.classList.remove('pause');
                this.innerText = direction === 1 ? '◄' : '►';
                paused = true;
                manual = true;
            } else {
                this.classList.add('pause');
                this.innerText = '❚❚';
                paused = false;
                manual = false;
                loopMe(true);
            }
            
            updateAddComments(!manual);
        };
        
        function manualPlay(){
            playButton.classList.remove('pause');
            playButton.innerText = direction === 1 ? '◄' : '►';
            
            paused = true;
            manual = true;
    
            updateAddComments(false);
            loopMe(true);
        }
        
        previousButton.onclick = function(){
            if (this.classList.contains('disabled')) {
                return
            }
            
            direction = 1;
            manualPlay();
        };
        
        nextButton.onclick = function(){
            if (this.classList.contains('disabled')) {
                return
            }
            
            direction = 0;
            manualPlay();
        };
        
        loopMe();
    }
}

function dateAgo(date, detailed){
    const now = new Date();
    let secondsDiff = Math.round((now - date) / 1000);
    
    const minuteSeconds = 60;
    const hourMinutes = minuteSeconds * 60;
    const dayHours = hourMinutes * 24;
    const monthDays = dayHours * 30;
    const yearMonths = monthDays * 12;
    
    let ago = [], x;
    
    function getAgo(s){
        if (s >= yearMonths) {
            x = Math.floor(s / yearMonths);
            ago.push({
                name: 'year' + ( x > 1 ? 's' : ''),
                value: x,
            });
            if (s % yearMonths){
                getAgo(s % yearMonths);
            }
        } else if (s >= monthDays) {
            x = Math.floor(s / monthDays);
            ago.push({
                name: 'month' + ( x > 1 ? 's' : ''),
                value: x,
            });
            if (s % monthDays){
                getAgo(s % monthDays);
            }
        } else if (s >= dayHours) {
            x = Math.floor(s / dayHours);
            ago.push({
                name: 'day' + ( x > 1 ? 's' : ''),
                value: x,
            });
            if (s % dayHours){
                getAgo(s % dayHours);
            }
        } else if (s >= hourMinutes) {
            x = Math.floor(s / hourMinutes);
            ago.push({
                name: 'hr' + ( x > 1 ? 's' : ''),
                value: x,
            });
            if (s % hourMinutes){
                getAgo(s % hourMinutes);
            }
        } else if (s >= minuteSeconds) {
            x = Math.floor(s / minuteSeconds);
            ago.push({
                name: 'min' + ( x > 1 ? 's' : ''),
                value: x,
            });
            if (s % minuteSeconds){
                getAgo(s % minuteSeconds);
            }
        } else {
            ago.push({
                name: 'second' + (s > 1 ? 's' : ''),
                value: s,
            });
        }
    }
    
    getAgo(secondsDiff);
    
    if (!detailed) {
        return ago[0].value + ' ' + ago[0].name
    }
    
    let str = "";
    ago.forEach((x) => {
        if (str !== ""){
            str += ' & ';
        }
        str += x.value + ' ' + x.name;
    });
    
    return str;
}

function readAsDataURL(file, callback){
    if (file.constructor.name.toLowerCase() === "file" && simo.is.function(callback)) {
        const fr = new FileReader();
        fr.onload = function(r) {
            callback(r.target.result)
        };
        
        fr.readAsDataURL(file);
    }
}

function fileUploadChanged(f){
    let p = f.parentNode.querySelector('.preview');
    p.innerHTML = '';
    
    for (let i in f.files) {
        if (f.files.hasOwnProperty(i)) {
            const file = f.files[i];
            
            let img = document.createElement('img');
            
            readAsDataURL(file, (dataURL) => {
                img.setAttribute('src', dataURL);
                img.setAttribute('name', file.name);
            });
            
            p.append(img);
        }
    }
}

function togglePostButton(el) {
    if (simo.is.empty(el.value)) {
        el.nextElementSibling.classList.add('disabled');
    } else {
        el.nextElementSibling.classList.remove('disabled');
    }
}

function projectImageCommentPost(el) {
    if (el.classList.contains('disabled')) {
        return
    }
    const commentField = el.previousElementSibling;
    const userUid = parseInt(el.getAttribute('data-user-uid')),
        imageUid = parseInt(el.getAttribute('data-image-uid')),
        comment = commentField.value;
    
    if (simo.is.number(userUid) && simo.is.number(imageUid) && !simo.is.empty(comment)) {
        const params = {
            userUid: userUid,
            projectImageUid: imageUid,
            comment: comment,
            sessionUser: getSessionUser()
        };
        
        simo.send.data("POST", params, simo.pcprejanApiUrl + "/project/image/comment", function (res) {
            simo.dataHandler(res, (json) => {
                getAllProjects(simo.pcp.active.project.uid, () => {
                    updateImageComments(imageUid)
                });
                commentField.value = '';
            }, null, true)
        }, true)
    }
}

Array.prototype.getWithUID = function(uid) {
    return this.find(a => a.uid === uid) || {}
};

Array.prototype.deleteWithUID = function(uid) {
    const targetIndex = this.findIndex(a => a.uid === uid);
    if (targetIndex >= 0) {
        this.splice(targetIndex, 1);
    }
};

Array.prototype.updateWithUID = function(uid, obj) {
    if (!simo.is.object(obj)) {
        error(obj, 'Not an object [ Array.prototype.updateWithUID ]');
        return
    }
    
    const targetIndex = this.findIndex(a => a.uid === uid);
    if (targetIndex >= 0) {
        for (const v in obj) {
            if (obj.hasOwnProperty(v) && this[targetIndex].hasOwnProperty(v)) {
                this[targetIndex][v] = obj[v];
            }
        }
        
        return this[targetIndex];
    }
};

Array.prototype.clone = function() {
    return JSON.parse(JSON.stringify(this));
};

Object.prototype.clone = function() {
    return JSON.parse(JSON.stringify(this));
};